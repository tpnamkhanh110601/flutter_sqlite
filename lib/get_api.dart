import 'package:dio/dio.dart';
import 'package:flutter_sqlite/classes/user/user.dart';

Dio dio = Dio();

Future<Response> getApi() async {
  final response = await dio.get('https://randomuser.me/api/');
  return response;
}

Future<User> convertToObject() async {
  final response = await getApi();
  if (response.statusCode == 200) {
    var data = response.data['results'][0];
    final result = User.fromJson(data);
    return result;
  } else {
    throw Exception('Get data Failed');
  }
}
