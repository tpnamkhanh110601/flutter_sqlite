import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_sqlite/add_user.dart';
import 'package:flutter_sqlite/database/users_table.dart';
import 'package:flutter_sqlite/classes/user/user.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const GetMaterialApp(
      home: Scaffold(body: HomePage()),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<User> users = [];

  void retrieveData() async {
    final data = await retrieveUsers();
    setState(() {
      users = data;
    });
  }

  @override
  void initState() {
    super.initState();
    getUser();
    retrieveData();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          children: [
            if (users.isEmpty) const CircularProgressIndicator(),
            if (users.isNotEmpty)
              ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: users.length,
                  itemBuilder: (context, index) {
                    return SizedBox(
                      height: 50,
                      child: Row(
                        children: [
                          SizedBox(
                            width: 150,
                            child: Text(
                                '${users[index].name.first} ${users[index].name.last}'),
                          ),
                          SizedBox(
                              width: 100, child: Text(users[index].gender)),
                          Text(users[index].location.country),
                        ],
                      ),
                    );
                  })
          ],
        ),
      ),
    );
  }
}
