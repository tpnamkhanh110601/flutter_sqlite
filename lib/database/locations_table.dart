import 'dart:ffi';

import 'package:flutter_sqlite/database/coordinates_table.dart';
import 'package:flutter_sqlite/database/streets_table.dart';
import 'package:flutter_sqlite/database/timezone_table.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/location/location.dart';
import 'package:flutter_sqlite/classes/street/street.dart';
import 'package:flutter_sqlite/classes/coordinates/coordinates.dart';
import 'package:flutter_sqlite/classes/timezone/timezone.dart';

import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertLocation(
    Location location, int streetId, int coordinatesId, int timezoneId) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  var data = location.toJson();
  data['street'] = streetId.toString();
  data['coordinates'] = coordinatesId.toString();
  data['timezone'] = timezoneId.toString();
  final insertedId = database.insert('locations', data);
  return insertedId;
}

Future<Location> retrieveALocation(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row =
      await database.query('locations', where: 'id = ?', whereArgs: [id]);
  Street street = await retrieveAStreet(int.parse(row[0]['street'].toString()));
  Coordinates coordinates =
      await retrieveACoordinates(int.parse(row[0]['coordinates'].toString()));
  TimeZone timeZone =
      await retrieveATimeZone(int.parse(row[0]['timezone'].toString()));

  return Location(
      street: street,
      city: row[0]['city'].toString(),
      state: row[0]['state'].toString(),
      country: row[0]['country'].toString(),
      postcode: row[0]['postcode'],
      coordinates: coordinates,
      timezone: timeZone);
}
