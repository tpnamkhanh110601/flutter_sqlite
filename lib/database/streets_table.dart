import 'dart:ffi';

import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/street/street.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertStreet(Street street) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  final insertedId = database.insert('streets', street.toJson());
  return insertedId;
}

Future<Street> retrieveAStreet(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row = await database.query('streets', where: 'id = ?', whereArgs: [id]);
  return Street(
      number: int.parse(row[0]['number'].toString()),
      name: row[0]['name'].toString());
}
