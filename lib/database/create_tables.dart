import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

Future<String> getDatabase() async {
  WidgetsFlutterBinding.ensureInitialized();

  final directory = await getDatabasesPath();
  final path = join(directory, 'sqlite.db');
  return path;
}

Future<void> createStreetsTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path, onCreate: (db, version) {
    return db.execute('CREATE TABLE streets('
        'id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'number INTEGER,'
        'name TEXT)');
  }, version: 1);
  await database.close();
}

Future<void> createCoordinatesTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE coordinates('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'latitude INTEGER,'
      'longitude INTEGER)');
  await database.close();
}

Future<void> createTimeZoneTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE timezones('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'offset TEXT,'
      'description TEXT)');
  await database.close();
}

Future<void> createLocationsTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE locations('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'street INTEGER,'
      'city TEXT,'
      'state TEXT,'
      'country TEXT,'
      'postcode TEXT,'
      'coordinates INTEGER,'
      'timezone INTEGER,'
      'FOREIGN KEY (street) REFERENCES streets(id),'
      'FOREIGN KEY (coordinates) REFERENCES coordinates(id),'
      'FOREIGN KEY (timezone) REFERENCES timezones(id))');
  await database.close();
}

Future<void> createNamesTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE names('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'title TEXT,'
      'first TEXT,'
      'last TEXT)');
  await database.close();
}

Future<void> createLoginsTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE logins('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'uuid TEXT,'
      'first TEXT,'
      'username TEXT,'
      'password TEXT,'
      'salt TEXT,'
      'md5 TEXT,'
      'sha1 TEXT,'
      'sha256 TEXT)');
  await database.close();
}

Future<void> createDobsTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE dobs('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'date TEXT,'
      'age INTEGER)');
  await database.close();
}

Future<void> createRegisteredsTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE registereds('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'date TEXT,'
      'age INTEGER)');
  await database.close();
}

Future<void> createIdsTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE ids('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'name TEXT,'
      'value TEXT)');
  await database.close();
}

Future<void> createPicturesTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE pictures('
      'id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'large TEXT,'
      'medium TEXT,'
      'thumbnail TEXT)');
  await database.close();
}

Future<void> createUsersTable() async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  await database.execute('CREATE TABLE users('
      'user_id INTEGER PRIMARY KEY AUTOINCREMENT,'
      'gender TEXT,'
      'name INTEGER,'
      'location INTEGER,'
      'email TEXT,'
      'login INTEGER,'
      'dob INTEGER,'
      'registered INTEGER,'
      'phone TEXT,'
      'cell TEXT,'
      'id INTEGER,'
      'picture INTEGER,'
      'nat TEXT)');
  await database.close();
}
