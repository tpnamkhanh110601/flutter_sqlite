import 'dart:ffi';

import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/coordinates/coordinates.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertCoordinate(Coordinates coordinates) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final insertedId = database.insert('coordinates', coordinates.toJson());
  return insertedId;
}

Future<Coordinates> retrieveACoordinates(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row =
      await database.query('coordinates', where: 'id = ?', whereArgs: [id]);
  return Coordinates(
      latitude: row[0]['latitude'].toString(),
      longitude: row[0]['longitude'].toString());
}
