import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/login/login.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertLogin(Login login) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  final insertedId = database.insert('logins', login.toJson());
  return insertedId;
}

Future<Login> retrieveALogin(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row = await database.query('logins', where: 'id = ?', whereArgs: [id]);

  return Login(
    uuid: row[0]['uuid'].toString(),
    username: row[0]['username'].toString(),
    password: row[0]['password'].toString(),
    salt: row[0]['salt'].toString(),
    md5: row[0]['md5'].toString(),
    sha1: row[0]['sha1'].toString(),
    sha256: row[0]['sha256'].toString(),
  );
}
