import 'package:flutter_sqlite/database/dobs_table.dart';
import 'package:flutter_sqlite/database/ids_table.dart';
import 'package:flutter_sqlite/database/locations_table.dart';
import 'package:flutter_sqlite/database/logins_table.dart';
import 'package:flutter_sqlite/database/names_table.dart';
import 'package:flutter_sqlite/database/pictures_table.dart';
import 'package:flutter_sqlite/database/registereds_table.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/user/user.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<void> insertUser(User user, int nameId, int locationId, int loginId,
    int dobId, int registeredId, int id, int pictureId) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  final data = user.toJson();
  data['name'] = nameId;
  data['location'] = locationId;
  data['login'] = loginId;
  data['dob'] = dobId;
  data['registered'] = registeredId;
  data['id'] = id;
  data['picture'] = pictureId;
  await database.insert('users', data);
}

Future<List<User>> retrieveUsers() async {
  List<User> list = [];
  final path = await getDatabase();
  final database = await openDatabase(path);
  final result = await database.query('users');

  result.forEach((element) async {
    var name = await retrieveAName(int.parse(element['name'].toString()));
    var location =
        await retrieveALocation(int.parse(element['location'].toString()));
    var login = await retrieveALogin(int.parse(element['login'].toString()));
    var dob = await retrieveADob(int.parse(element['dob'].toString()));
    var registered =
        await retrieveARegistered(int.parse(element['registered'].toString()));
    var id = await retrieveAId(int.parse(element['id'].toString()));
    var picture =
        await retrieveAPicture(int.parse(element['picture'].toString()));

    var user = User(
      gender: element['gender'].toString(),
      name: name,
      location: location,
      email: element['email'].toString(),
      login: login,
      dob: dob,
      registered: registered,
      phone: element['phone'].toString(),
      cell: element['cell'].toString(),
      id: id,
      picture: picture,
      nat: element['nat'].toString(),
    );
    list.add(user);
  });
  return list;
}
