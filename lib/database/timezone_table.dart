import 'dart:ffi';

import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/timezone/timezone.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertTimeZone(TimeZone timeZone) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final insertedId = database.insert('timezones', timeZone.toJson());
  return insertedId;
}

Future<TimeZone> retrieveATimeZone(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row =
      await database.query('timezones', where: 'id = ?', whereArgs: [id]);
  return TimeZone(
      offset: row[0]['offset'].toString(),
      description: row[0]['description'].toString());
}
