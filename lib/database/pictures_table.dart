import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/picture/picture.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertPicture(Picture picture) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  final insertedId = database.insert('pictures', picture.toJson());
  return insertedId;
}

Future<Picture> retrieveAPicture(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row = await database.query('dobs', where: 'id = ?', whereArgs: [id]);
  return Picture(
    large: row[0]['large'].toString(),
    medium: row[0]['medium'].toString(),
    thumbnail: row[0]['thumbnail'].toString(),
  );
}
