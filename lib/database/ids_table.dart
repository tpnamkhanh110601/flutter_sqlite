import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/id/id.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertId(Id id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  final insertedId = database.insert('ids', id.toJson());
  return insertedId;
}

Future<Id> retrieveAId(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row = await database.query('ids', where: 'id = ?', whereArgs: [id]);
  return Id(
    name: row[0]['name'].toString(),
    value: row[0]['value'].toString(),
  );
}
