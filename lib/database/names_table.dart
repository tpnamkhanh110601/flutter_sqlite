import 'dart:ffi';

import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/name/name.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertName(Name name) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  final insertedId = database.insert('names', name.toJson());
  return insertedId;
}

Future<Name> retrieveAName(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row = await database.query('names', where: 'id = ?', whereArgs: [id]);

  return Name(
    title: row[0]['title'].toString(),
    first: row[0]['first'].toString(),
    last: row[0]['last'].toString(),
  );
}
