import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/registered/registered.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertRegistered(Registered registered) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  final insertedId = database.insert('registereds', registered.toJson());
  return insertedId;
}

Future<Registered> retrieveARegistered(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row =
      await database.query('registereds', where: 'id = ?', whereArgs: [id]);
  return Registered(
    date: row[0]['date'].toString(),
    age: int.parse(row[0]['age'].toString()),
  );
}
