import 'package:sqflite/sqflite.dart';
import 'package:flutter_sqlite/classes/dob/dob.dart';
import 'package:flutter_sqlite/database/create_tables.dart';

Future<int> insertDob(Dob dob) async {
  final path = await getDatabase();
  final database = await openDatabase(path);

  final insertedId = database.insert('dobs', dob.toJson());
  return insertedId;
}

Future<Dob> retrieveADob(int id) async {
  final path = await getDatabase();
  final database = await openDatabase(path);
  final row = await database.query('dobs', where: 'id = ?', whereArgs: [id]);
  return Dob(
    date: row[0]['date'].toString(),
    age: int.parse(row[0]['age'].toString()),
  );
}
