import 'package:freezed_annotation/freezed_annotation.dart';

part 'dob.freezed.dart';

part 'dob.g.dart';

@freezed
class Dob with _$Dob {
  const factory Dob({
    required String date,
    required int age,
  }) = _Dob;

  factory Dob.fromJson(Map<String, Object?> json) => _$DobFromJson(json);
}
