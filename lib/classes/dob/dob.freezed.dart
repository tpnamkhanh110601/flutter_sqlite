// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'dob.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Dob _$DobFromJson(Map<String, dynamic> json) {
  return _Dob.fromJson(json);
}

/// @nodoc
mixin _$Dob {
  String get date => throw _privateConstructorUsedError;
  int get age => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DobCopyWith<Dob> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DobCopyWith<$Res> {
  factory $DobCopyWith(Dob value, $Res Function(Dob) then) =
      _$DobCopyWithImpl<$Res, Dob>;
  @useResult
  $Res call({String date, int age});
}

/// @nodoc
class _$DobCopyWithImpl<$Res, $Val extends Dob> implements $DobCopyWith<$Res> {
  _$DobCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = null,
    Object? age = null,
  }) {
    return _then(_value.copyWith(
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_DobCopyWith<$Res> implements $DobCopyWith<$Res> {
  factory _$$_DobCopyWith(_$_Dob value, $Res Function(_$_Dob) then) =
      __$$_DobCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String date, int age});
}

/// @nodoc
class __$$_DobCopyWithImpl<$Res> extends _$DobCopyWithImpl<$Res, _$_Dob>
    implements _$$_DobCopyWith<$Res> {
  __$$_DobCopyWithImpl(_$_Dob _value, $Res Function(_$_Dob) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = null,
    Object? age = null,
  }) {
    return _then(_$_Dob(
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Dob implements _Dob {
  const _$_Dob({required this.date, required this.age});

  factory _$_Dob.fromJson(Map<String, dynamic> json) => _$$_DobFromJson(json);

  @override
  final String date;
  @override
  final int age;

  @override
  String toString() {
    return 'Dob(date: $date, age: $age)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Dob &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.age, age) || other.age == age));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, date, age);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_DobCopyWith<_$_Dob> get copyWith =>
      __$$_DobCopyWithImpl<_$_Dob>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DobToJson(
      this,
    );
  }
}

abstract class _Dob implements Dob {
  const factory _Dob({required final String date, required final int age}) =
      _$_Dob;

  factory _Dob.fromJson(Map<String, dynamic> json) = _$_Dob.fromJson;

  @override
  String get date;
  @override
  int get age;
  @override
  @JsonKey(ignore: true)
  _$$_DobCopyWith<_$_Dob> get copyWith => throw _privateConstructorUsedError;
}
