// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dob.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Dob _$$_DobFromJson(Map<String, dynamic> json) => _$_Dob(
      date: json['date'] as String,
      age: json['age'] as int,
    );

Map<String, dynamic> _$$_DobToJson(_$_Dob instance) => <String, dynamic>{
      'date': instance.date,
      'age': instance.age,
    };
