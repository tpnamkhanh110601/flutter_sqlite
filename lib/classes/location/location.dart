import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter_sqlite/classes/street/street.dart';
import 'package:flutter_sqlite/classes/coordinates/coordinates.dart';
import 'package:flutter_sqlite/classes/timezone/timezone.dart';

part 'location.freezed.dart';

part 'location.g.dart';

@freezed
class Location with _$Location {
  const factory Location({
    required Street street,
    required String city,
    required String state,
    required String country,
    required dynamic postcode,
    required Coordinates coordinates,
    required TimeZone timezone,
  }) = _Location;

  factory Location.fromJson(Map<String, Object?> json) =>
      _$LocationFromJson(json);
}
