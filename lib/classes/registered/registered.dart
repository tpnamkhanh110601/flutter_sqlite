import 'package:freezed_annotation/freezed_annotation.dart';

part 'registered.freezed.dart';

part 'registered.g.dart';

@freezed
class Registered with _$Registered {
  const factory Registered({
    required String date,
    required int age,
  }) = _Registered;

  factory Registered.fromJson(Map<String, Object?> json) =>
      _$RegisteredFromJson(json);
}
