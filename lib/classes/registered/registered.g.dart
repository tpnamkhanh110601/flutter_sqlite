// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'registered.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Registered _$$_RegisteredFromJson(Map<String, dynamic> json) =>
    _$_Registered(
      date: json['date'] as String,
      age: json['age'] as int,
    );

Map<String, dynamic> _$$_RegisteredToJson(_$_Registered instance) =>
    <String, dynamic>{
      'date': instance.date,
      'age': instance.age,
    };
