import 'package:freezed_annotation/freezed_annotation.dart';

part 'id.freezed.dart';

part 'id.g.dart';

@freezed
class Id with _$Id {
  const factory Id({
    required String name,
    String? value,
  }) = _Id;

  factory Id.fromJson(Map<String, Object?> json) => _$IdFromJson(json);
}
