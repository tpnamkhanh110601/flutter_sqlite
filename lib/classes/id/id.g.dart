// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'id.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Id _$$_IdFromJson(Map<String, dynamic> json) => _$_Id(
      name: json['name'] as String,
      value: json['value'] as String?,
    );

Map<String, dynamic> _$$_IdToJson(_$_Id instance) => <String, dynamic>{
      'name': instance.name,
      'value': instance.value,
    };
