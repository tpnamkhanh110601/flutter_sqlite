// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'timezone.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TimeZone _$TimeZoneFromJson(Map<String, dynamic> json) {
  return _TimeZone.fromJson(json);
}

/// @nodoc
mixin _$TimeZone {
  String get offset => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TimeZoneCopyWith<TimeZone> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TimeZoneCopyWith<$Res> {
  factory $TimeZoneCopyWith(TimeZone value, $Res Function(TimeZone) then) =
      _$TimeZoneCopyWithImpl<$Res, TimeZone>;
  @useResult
  $Res call({String offset, String description});
}

/// @nodoc
class _$TimeZoneCopyWithImpl<$Res, $Val extends TimeZone>
    implements $TimeZoneCopyWith<$Res> {
  _$TimeZoneCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? offset = null,
    Object? description = null,
  }) {
    return _then(_value.copyWith(
      offset: null == offset
          ? _value.offset
          : offset // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_TimeZoneCopyWith<$Res> implements $TimeZoneCopyWith<$Res> {
  factory _$$_TimeZoneCopyWith(
          _$_TimeZone value, $Res Function(_$_TimeZone) then) =
      __$$_TimeZoneCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String offset, String description});
}

/// @nodoc
class __$$_TimeZoneCopyWithImpl<$Res>
    extends _$TimeZoneCopyWithImpl<$Res, _$_TimeZone>
    implements _$$_TimeZoneCopyWith<$Res> {
  __$$_TimeZoneCopyWithImpl(
      _$_TimeZone _value, $Res Function(_$_TimeZone) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? offset = null,
    Object? description = null,
  }) {
    return _then(_$_TimeZone(
      offset: null == offset
          ? _value.offset
          : offset // ignore: cast_nullable_to_non_nullable
              as String,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TimeZone implements _TimeZone {
  const _$_TimeZone({required this.offset, required this.description});

  factory _$_TimeZone.fromJson(Map<String, dynamic> json) =>
      _$$_TimeZoneFromJson(json);

  @override
  final String offset;
  @override
  final String description;

  @override
  String toString() {
    return 'TimeZone(offset: $offset, description: $description)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TimeZone &&
            (identical(other.offset, offset) || other.offset == offset) &&
            (identical(other.description, description) ||
                other.description == description));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, offset, description);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TimeZoneCopyWith<_$_TimeZone> get copyWith =>
      __$$_TimeZoneCopyWithImpl<_$_TimeZone>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TimeZoneToJson(
      this,
    );
  }
}

abstract class _TimeZone implements TimeZone {
  const factory _TimeZone(
      {required final String offset,
      required final String description}) = _$_TimeZone;

  factory _TimeZone.fromJson(Map<String, dynamic> json) = _$_TimeZone.fromJson;

  @override
  String get offset;
  @override
  String get description;
  @override
  @JsonKey(ignore: true)
  _$$_TimeZoneCopyWith<_$_TimeZone> get copyWith =>
      throw _privateConstructorUsedError;
}
