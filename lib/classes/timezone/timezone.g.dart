// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'timezone.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TimeZone _$$_TimeZoneFromJson(Map<String, dynamic> json) => _$_TimeZone(
      offset: json['offset'] as String,
      description: json['description'] as String,
    );

Map<String, dynamic> _$$_TimeZoneToJson(_$_TimeZone instance) =>
    <String, dynamic>{
      'offset': instance.offset,
      'description': instance.description,
    };
