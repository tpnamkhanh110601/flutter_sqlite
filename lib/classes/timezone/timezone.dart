import 'package:freezed_annotation/freezed_annotation.dart';

part 'timezone.freezed.dart';

part 'timezone.g.dart';

@freezed
class TimeZone with _$TimeZone {
  const factory TimeZone({
    required String offset,
    required String description,
  }) = _TimeZone;

  factory TimeZone.fromJson(Map<String, Object?> json) =>
      _$TimeZoneFromJson(json);
}
