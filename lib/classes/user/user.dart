import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter_sqlite/classes/name/name.dart';
import 'package:flutter_sqlite/classes/location/location.dart';
import 'package:flutter_sqlite/classes/login/login.dart';
import 'package:flutter_sqlite/classes/dob/dob.dart';
import 'package:flutter_sqlite/classes/registered/registered.dart';
import 'package:flutter_sqlite/classes/id/id.dart';
import 'package:flutter_sqlite/classes/picture/picture.dart';

part 'user.freezed.dart';

part 'user.g.dart';

@freezed
class User with _$User {
  const factory User({
    required String gender,
    required Name name,
    required Location location,
    required String email,
    required Login login,
    required Dob dob,
    required Registered registered,
    required String phone,
    required String cell,
    required Id id,
    required Picture picture,
    required String nat,
  }) = _User;

  factory User.fromJson(Map<String, Object?> json) => _$UserFromJson(json);
}
