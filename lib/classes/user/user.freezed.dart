// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

User _$UserFromJson(Map<String, dynamic> json) {
  return _User.fromJson(json);
}

/// @nodoc
mixin _$User {
  String get gender => throw _privateConstructorUsedError;
  Name get name => throw _privateConstructorUsedError;
  Location get location => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  Login get login => throw _privateConstructorUsedError;
  Dob get dob => throw _privateConstructorUsedError;
  Registered get registered => throw _privateConstructorUsedError;
  String get phone => throw _privateConstructorUsedError;
  String get cell => throw _privateConstructorUsedError;
  Id get id => throw _privateConstructorUsedError;
  Picture get picture => throw _privateConstructorUsedError;
  String get nat => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserCopyWith<User> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res, User>;
  @useResult
  $Res call(
      {String gender,
      Name name,
      Location location,
      String email,
      Login login,
      Dob dob,
      Registered registered,
      String phone,
      String cell,
      Id id,
      Picture picture,
      String nat});

  $NameCopyWith<$Res> get name;
  $LocationCopyWith<$Res> get location;
  $LoginCopyWith<$Res> get login;
  $DobCopyWith<$Res> get dob;
  $RegisteredCopyWith<$Res> get registered;
  $IdCopyWith<$Res> get id;
  $PictureCopyWith<$Res> get picture;
}

/// @nodoc
class _$UserCopyWithImpl<$Res, $Val extends User>
    implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gender = null,
    Object? name = null,
    Object? location = null,
    Object? email = null,
    Object? login = null,
    Object? dob = null,
    Object? registered = null,
    Object? phone = null,
    Object? cell = null,
    Object? id = null,
    Object? picture = null,
    Object? nat = null,
  }) {
    return _then(_value.copyWith(
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      location: null == location
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as Location,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      login: null == login
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as Login,
      dob: null == dob
          ? _value.dob
          : dob // ignore: cast_nullable_to_non_nullable
              as Dob,
      registered: null == registered
          ? _value.registered
          : registered // ignore: cast_nullable_to_non_nullable
              as Registered,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      cell: null == cell
          ? _value.cell
          : cell // ignore: cast_nullable_to_non_nullable
              as String,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as Id,
      picture: null == picture
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as Picture,
      nat: null == nat
          ? _value.nat
          : nat // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $NameCopyWith<$Res> get name {
    return $NameCopyWith<$Res>(_value.name, (value) {
      return _then(_value.copyWith(name: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $LocationCopyWith<$Res> get location {
    return $LocationCopyWith<$Res>(_value.location, (value) {
      return _then(_value.copyWith(location: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $LoginCopyWith<$Res> get login {
    return $LoginCopyWith<$Res>(_value.login, (value) {
      return _then(_value.copyWith(login: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $DobCopyWith<$Res> get dob {
    return $DobCopyWith<$Res>(_value.dob, (value) {
      return _then(_value.copyWith(dob: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $RegisteredCopyWith<$Res> get registered {
    return $RegisteredCopyWith<$Res>(_value.registered, (value) {
      return _then(_value.copyWith(registered: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $IdCopyWith<$Res> get id {
    return $IdCopyWith<$Res>(_value.id, (value) {
      return _then(_value.copyWith(id: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $PictureCopyWith<$Res> get picture {
    return $PictureCopyWith<$Res>(_value.picture, (value) {
      return _then(_value.copyWith(picture: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_UserCopyWith<$Res> implements $UserCopyWith<$Res> {
  factory _$$_UserCopyWith(_$_User value, $Res Function(_$_User) then) =
      __$$_UserCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String gender,
      Name name,
      Location location,
      String email,
      Login login,
      Dob dob,
      Registered registered,
      String phone,
      String cell,
      Id id,
      Picture picture,
      String nat});

  @override
  $NameCopyWith<$Res> get name;
  @override
  $LocationCopyWith<$Res> get location;
  @override
  $LoginCopyWith<$Res> get login;
  @override
  $DobCopyWith<$Res> get dob;
  @override
  $RegisteredCopyWith<$Res> get registered;
  @override
  $IdCopyWith<$Res> get id;
  @override
  $PictureCopyWith<$Res> get picture;
}

/// @nodoc
class __$$_UserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res, _$_User>
    implements _$$_UserCopyWith<$Res> {
  __$$_UserCopyWithImpl(_$_User _value, $Res Function(_$_User) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gender = null,
    Object? name = null,
    Object? location = null,
    Object? email = null,
    Object? login = null,
    Object? dob = null,
    Object? registered = null,
    Object? phone = null,
    Object? cell = null,
    Object? id = null,
    Object? picture = null,
    Object? nat = null,
  }) {
    return _then(_$_User(
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as Name,
      location: null == location
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as Location,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      login: null == login
          ? _value.login
          : login // ignore: cast_nullable_to_non_nullable
              as Login,
      dob: null == dob
          ? _value.dob
          : dob // ignore: cast_nullable_to_non_nullable
              as Dob,
      registered: null == registered
          ? _value.registered
          : registered // ignore: cast_nullable_to_non_nullable
              as Registered,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      cell: null == cell
          ? _value.cell
          : cell // ignore: cast_nullable_to_non_nullable
              as String,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as Id,
      picture: null == picture
          ? _value.picture
          : picture // ignore: cast_nullable_to_non_nullable
              as Picture,
      nat: null == nat
          ? _value.nat
          : nat // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_User implements _User {
  const _$_User(
      {required this.gender,
      required this.name,
      required this.location,
      required this.email,
      required this.login,
      required this.dob,
      required this.registered,
      required this.phone,
      required this.cell,
      required this.id,
      required this.picture,
      required this.nat});

  factory _$_User.fromJson(Map<String, dynamic> json) => _$$_UserFromJson(json);

  @override
  final String gender;
  @override
  final Name name;
  @override
  final Location location;
  @override
  final String email;
  @override
  final Login login;
  @override
  final Dob dob;
  @override
  final Registered registered;
  @override
  final String phone;
  @override
  final String cell;
  @override
  final Id id;
  @override
  final Picture picture;
  @override
  final String nat;

  @override
  String toString() {
    return 'User(gender: $gender, name: $name, location: $location, email: $email, login: $login, dob: $dob, registered: $registered, phone: $phone, cell: $cell, id: $id, picture: $picture, nat: $nat)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_User &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.location, location) ||
                other.location == location) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.login, login) || other.login == login) &&
            (identical(other.dob, dob) || other.dob == dob) &&
            (identical(other.registered, registered) ||
                other.registered == registered) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.cell, cell) || other.cell == cell) &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.picture, picture) || other.picture == picture) &&
            (identical(other.nat, nat) || other.nat == nat));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, gender, name, location, email,
      login, dob, registered, phone, cell, id, picture, nat);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserCopyWith<_$_User> get copyWith =>
      __$$_UserCopyWithImpl<_$_User>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserToJson(
      this,
    );
  }
}

abstract class _User implements User {
  const factory _User(
      {required final String gender,
      required final Name name,
      required final Location location,
      required final String email,
      required final Login login,
      required final Dob dob,
      required final Registered registered,
      required final String phone,
      required final String cell,
      required final Id id,
      required final Picture picture,
      required final String nat}) = _$_User;

  factory _User.fromJson(Map<String, dynamic> json) = _$_User.fromJson;

  @override
  String get gender;
  @override
  Name get name;
  @override
  Location get location;
  @override
  String get email;
  @override
  Login get login;
  @override
  Dob get dob;
  @override
  Registered get registered;
  @override
  String get phone;
  @override
  String get cell;
  @override
  Id get id;
  @override
  Picture get picture;
  @override
  String get nat;
  @override
  @JsonKey(ignore: true)
  _$$_UserCopyWith<_$_User> get copyWith => throw _privateConstructorUsedError;
}
