// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'street.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Street _$$_StreetFromJson(Map<String, dynamic> json) => _$_Street(
      number: json['number'] as int,
      name: json['name'] as String,
    );

Map<String, dynamic> _$$_StreetToJson(_$_Street instance) => <String, dynamic>{
      'number': instance.number,
      'name': instance.name,
    };
