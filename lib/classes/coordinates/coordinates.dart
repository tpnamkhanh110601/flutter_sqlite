import 'package:freezed_annotation/freezed_annotation.dart';

part 'coordinates.freezed.dart';

part 'coordinates.g.dart';

@freezed
class Coordinates with _$Coordinates {
  const factory Coordinates({
    required String latitude,
    required String longitude,
  }) = _Coordinates;

  factory Coordinates.fromJson(Map<String, Object?> json) =>
      _$CoordinatesFromJson(json);
}
