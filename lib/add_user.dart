import 'package:flutter_sqlite/database/ids_table.dart';
import 'package:flutter_sqlite/database/pictures_table.dart';
import 'package:flutter_sqlite/database/registereds_table.dart';
import 'package:flutter_sqlite/database/users_table.dart';
import 'package:flutter_sqlite/get_api.dart';
import 'package:flutter_sqlite/database/logins_table.dart';
import 'package:flutter_sqlite/database/names_table.dart';
import 'package:flutter_sqlite/database/create_tables.dart';
import 'package:flutter_sqlite/database/streets_table.dart';
import 'package:flutter_sqlite/database/coordinates_table.dart';
import 'package:flutter_sqlite/database/locations_table.dart';
import 'package:flutter_sqlite/database/timezone_table.dart';
import 'package:flutter_sqlite/database/dobs_table.dart';

var user;

void getUser() async {
  user = await convertToObject();
  final streetId = await insertStreet(user.location.street);
  final timezoneId = await insertTimeZone(user.location.timezone);
  final coordinatesId = await insertCoordinate(user.location.coordinates);
  final locationId =
      await insertLocation(user.location, streetId, coordinatesId, timezoneId);
  final nameId = await insertName(user.name);
  final loginId = await insertLogin(user.login);
  final dobId = await insertDob(user.dob);
  final registeredId = await insertRegistered(user.registered);
  final id = await insertId(user.id);
  final pictureId = await insertPicture(user.picture);
  await insertUser(
      user, nameId, locationId, loginId, dobId, registeredId, id, pictureId);
}

void getUserData() async {
  await getApi();
}

void createTable() async {
  await createUsersTable();
  await createTimeZoneTable();
  await createStreetsTable();
  await createCoordinatesTable();
  await createLocationsTable();
  await createNamesTable();
  await createLoginsTable();
  await createDobsTable();
  await createRegisteredsTable();
  await createIdsTable();
  await createPicturesTable();
}
